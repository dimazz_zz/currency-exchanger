//
//  CurrencyApiManagerTests.swift
//  CurrencyExchangerTests
//
//  Created by Dmitry Li on 09/03/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import XCTest
@testable import CurrencyExchanger

class CurrencyApiManagerTests: XCTestCase {

    var currencyApiManager: CurrencyApiManager! = CurrencyApiManager()
    
    override func setUp() {
        currencyApiManager.serverCaller.configuration.protocolClasses = [URLProtocolMock.self]
    }

    override func tearDown() {
        currencyApiManager = nil
    }
    
    func testFetchingCurrenciesForNilError() {
        currencyApiManager.performLoadingCurrency(with: "EUR") { (currencyRates, error) in
            XCTAssertNil(error)
        }
    }

    func testFetchingCurrenciesForNotNilCurrencyRates() {
        currencyApiManager.performLoadingCurrency(with: "EUR") { (currencyRates, error) in
            XCTAssertNotNil(currencyRates)
            XCTAssertNotNil(currencyRates?.rates)
        }
    }
    
    func testFetchingCurrenciesForCurrecyRatesBase() {
        currencyApiManager.performLoadingCurrency(with: "EUR") { (currencyRates, error) in
            XCTAssert(currencyRates?.base == "EUR")
        }
    }
    
    func testFetchingCurrenciesForNotContainingBaseInCurrencyRates() {
        currencyApiManager.performLoadingCurrency(with: "EUR") { (currencyRates, error) in
            XCTAssertFalse(currencyRates?.rates.contains(where: {$0.name == "EUR"}) ?? false)
        }
    }
    
    func testFetchingCurrenciesWithInvalidBase() {
        currencyApiManager.performLoadingCurrency(with: "123") { (currencyRates, error) in
            XCTAssertNotNil(error)
            XCTAssertNil(currencyRates)
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
