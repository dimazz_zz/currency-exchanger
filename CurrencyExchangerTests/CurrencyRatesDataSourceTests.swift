//
//  CurrencyRatesDataSourceTests.swift
//  CurrencyExchangerTests
//
//  Created by Dmitry Li on 05/03/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import XCTest
@testable import CurrencyExchanger

class CurrencyRatesDataSourceTests: XCTestCase {
    var dataProvider: CurrencyRatesDataProvider! = CurrencyRatesDataProvider()
    var dataSource: CurrencyRatesDataSource!
    var currencyViewController: CurrencyExchangerTableViewController!

    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        currencyViewController = storyboard.instantiateInitialViewController() as? CurrencyExchangerTableViewController
        currencyViewController.loadView()
        let currencyRates = [Currency(with: "USD", value: 1.235), Currency(with: "RUB", value: 79.15), Currency(with: "THB", value: 38.167), Currency(with: "AUD", value: 1.635)]
        
        let baseCurrency = Currency(with: "EUR", value: 1.0)
        var rates = [Currency]()
        rates.append(baseCurrency)
        rates.append(contentsOf: currencyRates)
        dataProvider.update(with: rates)
        dataSource = CurrencyRatesDataSource(with: dataProvider)
    }

    override func tearDown() {
        dataProvider = nil
        dataSource = nil
        currencyViewController = nil
    }

    func testNumberOfRows() {
        XCTAssert(dataSource.tableView(currencyViewController.tableView, numberOfRowsInSection: 0) == dataProvider.numberOfCurrencies())
    }
    
    func testCellForRow() {
        for i in 0..<dataProvider.numberOfCurrencies() {
            let indexPath = IndexPath(row: i, section: 0)
            let cell = dataSource.tableView(currencyViewController.tableView, cellForRowAt: indexPath) as? CurrencyTableViewCell
            XCTAssertNotNil(cell)
            XCTAssert(cell?.currencyName == dataProvider.currencyName(at: i))
            XCTAssert(cell?.currencyValue == dataProvider.currencyValue(at: i))
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
