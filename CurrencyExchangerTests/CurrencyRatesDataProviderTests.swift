//
//  CurrencyRatesDataProviderTests.swift
//  CurrencyExchangerTests
//
//  Created by Dmitry Li on 02/03/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import XCTest
@testable import CurrencyExchanger

class CurrencyRatesDataProviderTests: XCTestCase {
    var currencyRates: [Currency]!
    var currencyRatesDataProvider: CurrencyRatesDataProvider! = CurrencyRatesDataProvider()
    var baseCurrency: Currency!
    
    override func setUp() {
        baseCurrency = Currency(with: "EUR", value: 1.0)
        currencyRates = [Currency(with: "USD", value: 1.235), Currency(with: "RUB", value: 79.15), Currency(with: "THB", value: 38.167), Currency(with: "AUD", value: 1.635)]
        var rates = [Currency]()
        rates.append(baseCurrency)
        rates.append(contentsOf: currencyRates)
        currencyRatesDataProvider.update(with: rates)
    }

    override func tearDown() {
        currencyRates = nil
        currencyRatesDataProvider = nil
        baseCurrency = nil
    }
    
    func testNumberOfCurrencies()  {
        XCTAssert(currencyRatesDataProvider.numberOfCurrencies() == currencyRates.count + 1)
    }
    
    func testCurrencyValues() {
        for i in 1..<currencyRates.count {
            XCTAssert(currencyRatesDataProvider.currencyValue(at: i) == currencyRates[i - 1].value)
        }
    }
    
    func testCurrencyNames() {
        for i in 1..<currencyRates.count {
            XCTAssert(currencyRatesDataProvider.currencyName(at: i) == currencyRates[i - 1].name)
        }
    }
    
    func testBaseCurrencyValue() {
        XCTAssert(currencyRatesDataProvider.currencyValue(at: 0) == baseCurrency.value)
    }
    
    func testBaseCurrencyName() {
        XCTAssert(currencyRatesDataProvider.currencyName(at: 0) == baseCurrency.name)
    }
    
    func testCurrencyAtIndex() {
        let currency = currencyRatesDataProvider.currency(at: 3)
        XCTAssert(currency?.name == "THB")
        XCTAssert(currency?.value == 38.167)
    }
    
    func testMovingCurrencyToTop() {
        currencyRatesDataProvider.moveCurrencyToTop(from: 2)
        XCTAssert(currencyRatesDataProvider.currencyName(at: 0) == "RUB")
    }
    
    func testCanEditingCurrency() {
        XCTAssert(currencyRatesDataProvider.canEditCurrency(at: 0))
        XCTAssertFalse(currencyRatesDataProvider.canEditCurrency(at: 1))
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
