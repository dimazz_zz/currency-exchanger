//
//  URLProtocolMock.swift
//  CurrencyExchangerTests
//
//  Created by Dmitry Li on 09/03/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class URLProtocolMock: URLProtocol {
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    // ignore this method; just send back what we were given
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        // if we have a valid URL…
//        if let url = request.url {
//            // …and if we have test data for that URL…
//            if let data = URLProtocolMock.testURLs[url] {
//                // …load it immediately.
//                self.client?.urlProtocol(self, didLoad: data)
//            }
        //        }

        
        if let url = request.url {
            let jsonDict: [String : Any]
            if url.query == "base=EUR" {
                jsonDict = ["base": "EUR",
                                "date": "2018-09-06",
                                "rates": ["AUD": 1.6223,
                                          "BGN": 1.9629,
                                          "BRL": 4.8092,
                                          "CAD": 1.5394,
                                          "CHF": 1.1316]] as [String : Any]
            } else {
                jsonDict = ["error" : "Invalid base"] as [String : Any]
            }
            let jsonData = try? JSONSerialization.data(withJSONObject: jsonDict, options: [])
            if let data = jsonData {
                self.client?.urlProtocol(self, didLoad: data)
            }
        }
        // mark that we've finished
        self.client?.urlProtocolDidFinishLoading(self)
    }
    
    // this method is required but doesn't need to do anything
    override func stopLoading() { }
}
