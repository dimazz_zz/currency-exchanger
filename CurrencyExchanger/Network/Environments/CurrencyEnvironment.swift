//
//  CurrencyEnvironment.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import Foundation

struct CurrencyEnvironment {}

extension CurrencyEnvironment: NetworkEnvironmentType {
    static var host: String! {
       return "revolut.duckdns.org"
    }
}
