//
//  Serializable.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import Foundation

protocol Serializable {
    static func serialize(from data: Data) -> Self?
}
