//
//  NetworkEnvironmentType.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import Foundation

protocol NetworkEnvironmentType {
    static var scheme: String! {get}
    static var host: String! {get}
    static var port: Int? {get}
    static var subPath: String {get}
    static var headers:[String: String]? {get}
}

extension NetworkEnvironmentType {
    static var scheme: String! {
        return "https"
    }
    
    static var port: Int? {
        return nil
    }
    
    static var subPath: String {
        return String()
    }
    
    static var headers: [String : String]? {
        return nil
    }
}
