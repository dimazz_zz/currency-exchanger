//
//  ServerCaller.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit
import os.log

public typealias HTTPCode = Int

public struct ServerResponse {
    var httpCode: HTTPCode? = 200
    var body: Data? = nil
    var error: Error? = nil
    
    var debugDescription: String {
        return "httpCode: \(httpCode ?? 200),  body: \(String(data: (body ?? Data()), encoding: .utf8) ?? "nil"), error: \(String(describing: error))"
    }
}

public struct ServerRequest {
    
    enum BodyKey: String {
        case array = "ArrayBodyKey"
        case arraySeparator = "__"
    }
        
    let endPoint: String
    let httpMethod: HTTPMethod
    var body: [String: String]? = nil
    var queryParameters: [String: Any?]? = nil
    
    var debugDescription: String {
        return "endPont: \(endPoint), verb: \(httpMethod.rawValue), query: \(String(describing: queryParameters)), body: \(String(describing: body)))"
    }
}

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    case update = "UPDATE"
    case patch = "PATCH"
    case put = "PUT"
}

class ServerCaller<E: NetworkEnvironmentType>: NSObject, URLSessionDelegate {
    
    //MARK: Public
    func perform(serverRequest request: ServerRequest, completion: @escaping ((_ response: ServerResponse) -> Void)) {
        var components = self.urlComponents
        components.path = E.subPath + request.endPoint
        components.queryItems = urlQueryItems(fromParameters: request.queryParameters)
        
        guard let url = components.url else {
            fatalError("cannot create url")
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpBody = generate(requestBody: request.body)
        urlRequest.httpMethod = request.httpMethod.rawValue
        var headers = E.headers
        if let _ = headers, request.httpMethod == .post {
            headers!["Content-Type"] = "application/x-www-form-urlencoded"
        }
        urlRequest.allHTTPHeaderFields = E.headers
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        session.dataTask(with: urlRequest) { (data: Data?, urlResponse: URLResponse?, error: Error?) in
            var response = ServerResponse()
            response.body = data
            response.error = error
            if let httpURLResponse = urlResponse as? HTTPURLResponse {
                response.httpCode = httpURLResponse.statusCode
            }
            completion(response)
        }.resume()
    }
        
    //MARK: Private
    private lazy var urlComponents: URLComponents = {
        var urlComponents = URLComponents()
        urlComponents.scheme = E.scheme
        urlComponents.host = E.host
        urlComponents.port = E.port
        return urlComponents
    }()
    
    public lazy var configuration: URLSessionConfiguration = {
        var conf = URLSessionConfiguration.default
        conf.httpAdditionalHeaders = ["User-Agent": userAgent]
        return conf
    }()
    
    private lazy var userAgent: String = {
        let appName = "CurrencyExchanger"
        guard let info = Bundle.main.infoDictionary else {
            return appName
        }
        let version = ((info["CFBundleShortVersionString"] ?? info[kCFBundleVersionKey as String]) as? String) ?? "Unkown version"
        let model = UIDevice.current.model
        let systemVersion = UIDevice.current.systemVersion
        let scale = Int(UIScreen.main.scale)
        var ua: String = "\(appName)/\(version) (\(model); iOS \(systemVersion); Scale/\(scale)"
        return ua
    }()
    
    private func urlQueryItems(fromParameters parameters:[String: Any?]?) -> [URLQueryItem]? {
        guard let parameters = parameters else {
            return nil
        }
        var queryItems = [URLQueryItem]()
        for (name, value) in parameters {
            var v: String?
            if let val = value {
                v = "\(val)"
            }
            let qi = URLQueryItem(name: name, value: v)
            queryItems.append(qi)
        }
        return queryItems
    }
    
    private func generate(requestBody body: [String: String]?) -> Data? {
        guard var b = body else {
            return nil
        }
        var parameters = [String]()
        if let listString = b[ServerRequest.BodyKey.array.rawValue] {
            let list = listString.components(separatedBy: ServerRequest.BodyKey.arraySeparator.rawValue)
            if let key = list.last {
                let values = Array(list.prefix(upTo: list.count - 1))
                for v in values {
                    parameters.append("\(key)=\(v)")
                }
            }
            b.removeValue(forKey: ServerRequest.BodyKey.array.rawValue)
        }
        let params = b.map { (key, value) -> String in
            return "\(key)=\(value)"
        }
        parameters += params
        var string: String! = parameters.joined(separator: "&")
        let set = CharacterSet(charactersIn: "+").inverted
        string = string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)?.addingPercentEncoding(withAllowedCharacters: set)
        return string.data(using: .utf8)
    }
    
    //MARK: URLSession delegate
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodClientCertificate) {
            completionHandler(.rejectProtectionSpace, nil)
        }
        if (challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust) {
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            completionHandler(.useCredential, credential)
        }
    }
}
