//
//  CurrencyApiManager.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class CurrencyApiManager: NSObject {
    //MARK: - Public
    func performLoadingCurrency(with base: String, complete: ((CurrencyRates?, Error?)->Void)?)  {
        let queryParameters = ["base" : base] as [String : Any]
        let request = ServerRequest(endPoint: "/latest", httpMethod: .get, body: nil, queryParameters: queryParameters)
        serverCaller.perform(serverRequest: request) { (response) in
            let serialiser = ServerResponseSerializer()
            do {
                let currencyRates: CurrencyRates? = try serialiser.serialize(fromResponse: response)
                complete?(currencyRates, nil)
            } catch {
                complete?(nil, error)
            }
        }
    }
    
    //MARK: - Private
    public lazy var serverCaller = ServerCaller<CurrencyEnvironment>()
}
