//
//  ServerResponseSerializer.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class ServerResponseSerializer {
    open func serialize<T: Serializable>(fromResponse response: ServerResponse) throws -> T?  {
        guard let data = response.body else {
            return nil
        }
        if let error = ServerError.serialize(from: data) {
            throw error
        } else {
            let result = T.serialize(from: data)
            return result
        }
    }
    
    open func serialize<Y: Codable>(type: Y.Type, from response: ServerResponse) throws -> Y? {
        guard let data = response.body else {
            return nil
        }
        if let error = ServerError.serialize(from: data) {
            throw error
        } else {
            let decoder = JSONDecoder()
            do {
                let result = try decoder.decode(type, from: data)
                return result
            }
        }
    }
}
