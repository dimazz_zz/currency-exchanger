//
//  ServerError.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

struct ServerError: Error, Decodable, Serializable {
    let code: String
    let description: String
    let title: String?
    let message: String?
    
    enum CodingKeys: String, CodingKey{
        case code
        case description = "desc"
        case title
        case message
    }
}

extension ServerError {
    static func serialize(from data: Data) -> ServerError? {
        let decoder = JSONDecoder()
        var error = try? decoder.decode(ServerError.self, from: data)
        if error == nil {
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
                let errorDesc = json!["error"] as? String {
                error = ServerError(code: "400", description: errorDesc, title: nil, message: nil)
            }
        }
        return error
    }
}
