//
//  CurrencyRates.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class CurrencyRates {
    let base: String
    let rates: [Currency]
    
    required init(with base: String, rates: [Currency]) {
        self.base = base
        self.rates = rates
    }
}

extension CurrencyRates: Serializable {
    static func serialize(from data: Data) -> Self? {
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any],
            let currenciesDict = json?["rates"] as? [String : Double],
            let base = json?["base"] as? String else {
            return nil
        }
        var rates = [Currency]()
        for (name, value) in currenciesDict {
            rates.append(Currency(with: name, value: value))
        }
        return self.init(with: base, rates: rates)
    }
}
