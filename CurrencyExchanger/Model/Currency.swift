//
//  Currency.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 23/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class Currency {
    var name: String
    var value: Double
    
    init(with name: String, value: Double) {
        self.name = name
        self.value = value
    }
    
    func multiplyValue(to baseValue: Double) {
        value *= baseValue
    }
}
