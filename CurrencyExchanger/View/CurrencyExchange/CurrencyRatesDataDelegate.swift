//
//  CurrencyRatesDataDelegate.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 02/03/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

protocol CurrencyRatesMoveProtocol:class {
    func currencyRatesDataDelegate(_ currencyRatesDataDelegate: CurrencyRatesDataDelegate, moveCurrencyToTopAt index: Int)
}

class CurrencyRatesDataDelegate: NSObject {
    //MARK: - Public
    public weak var moveDelegate: CurrencyRatesMoveProtocol?
    
    init(with currencyRatesDataProvider: CurrencyRatesDataProvider) {
        self.ratesDataProvider = currencyRatesDataProvider
    }
    
    //MARK: - Private
    fileprivate var ratesDataProvider: CurrencyRatesDataProvider
}

//MARK: - UITableViewDelegate
extension CurrencyRatesDataDelegate: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let firstIndexPath = IndexPath(row: 0, section: 0)
        
        tableView.cellForRow(at: firstIndexPath)?.resignFirstResponder()
        CATransaction.begin()
        tableView.beginUpdates()
        CATransaction.setCompletionBlock {
            tableView.resignFirstResponder()
            tableView.cellForRow(at: firstIndexPath)?.becomeFirstResponder()
        }

        tableView.moveRow(at: indexPath, to: firstIndexPath)
        tableView.endUpdates()
        CATransaction.commit()
        
        moveDelegate?.currencyRatesDataDelegate(self, moveCurrencyToTopAt: indexPath.row)
    }
}
