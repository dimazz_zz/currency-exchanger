//
//  CurrencyExchangerViewModel.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 27/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

protocol CurrencyExchangerViewModelDelegate: class {
    func didUpdateRates(_ currencyExchangerViewModel: CurrencyExchangerViewModel)
    func didSelectCurrency(_ currencyExchangerViewModel: CurrencyExchangerViewModel)
}

class CurrencyExchangerViewModel: NSObject {
    //MARK: - Public
    public weak var delegate: CurrencyExchangerViewModelDelegate?
    public lazy var ratesDataSource: CurrencyRatesDataSource = {
        let dataSource = CurrencyRatesDataSource(with: ratesDataProvider)
        dataSource.baseCurrencyDelegate = self
        return dataSource
    }()
    public lazy var ratesDataDelegate: CurrencyRatesDataDelegate = {
        let dataDelegate = CurrencyRatesDataDelegate(with: ratesDataProvider)
        dataDelegate.moveDelegate = self
        return dataDelegate
    }()
    
    public func startContiniousFetchingCurrencies(complete: ((CurrencyRates?, Error?)->Void)?) {
        timer = scheduleFetchingTimer()
    }
    
    public func pauseFetchingCurrencies() {
        timer.invalidate()
        self.dispatchResumeFetching?.cancel()
        self.dispatchResumeFetching = DispatchWorkItem { [unowned self] in
            self.timer = self.scheduleFetchingTimer()
        }
        if let dispatchTask = dispatchResumeFetching {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: dispatchTask)
        }
    }
    
    public func resumeFetchingCurrenciesIfNeeded() {
        timer.invalidate()
        if let dispatchTask = dispatchResumeFetching {
            dispatchTask.cancel()
            self.dispatchResumeFetching = DispatchWorkItem { [unowned self] in
                self.timer = self.scheduleFetchingTimer()
            }
            if let dispatchTask = dispatchResumeFetching {
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: dispatchTask)
            }
        }
    }
    
    //MARK: - Private
    fileprivate var currentCurrency = Currency(with: "EUR", value: 1.0)
    fileprivate var timer: Timer!
    fileprivate lazy var currencyApiManager = CurrencyApiManager()
    fileprivate lazy var ratesDataProvider = CurrencyRatesDataProvider()
    fileprivate var dispatchResumeFetching: DispatchWorkItem?
    
    private func scheduleFetchingTimer() -> Timer {
        return Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [unowned self] (timer) in
            self.currencyApiManager.performLoadingCurrency(with: self.currentCurrency.name,
                                                           complete:
                { [unowned self] (currencyRates, error) in
                    if let currencyRates = currencyRates {
                        var rates = [Currency]()
                        rates.append(self.currentCurrency)
                        rates.append(contentsOf: currencyRates.rates.map({
                            $0.value *= self.currentCurrency.value
                            return $0
                        }))
                        self.ratesDataProvider.update(with: rates)
                        self.delegate?.didUpdateRates(self)
                    }
            })
        }
    }
}

//MARK: - CurrencyTableViewCellDelegate
extension CurrencyExchangerViewModel: CurrencyTableViewCellDelegate {
    func currencyTableViewCell(_: CurrencyTableViewCell, didChangeCurrencyWith value: Double) {
        currentCurrency.value = value
    }
}

//MARK: - CurrencyRatesMoveProtocol
extension CurrencyExchangerViewModel: CurrencyRatesMoveProtocol {
    func currencyRatesDataDelegate(_ currencyRatesDataDelegate: CurrencyRatesDataDelegate,
                                   moveCurrencyToTopAt index: Int) {
        if let currency = ratesDataProvider.currency(at: index) {
            pauseFetchingCurrencies()
            currentCurrency = currency
            ratesDataProvider.moveCurrencyToTop(from: index)
            delegate?.didSelectCurrency(self)
        }
    }
}
