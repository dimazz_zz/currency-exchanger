//
//  CurrencyTableViewCell.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 28/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

protocol CurrencyTableViewCellDelegate: class {
    func currencyTableViewCell(_ : CurrencyTableViewCell, didChangeCurrencyWith value: Double)
}

class CurrencyTableViewCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet fileprivate var currencyNameLabel: UILabel!
    @IBOutlet fileprivate var currencyValueTextField: UITextField!

    //MARK: - Public
    public weak var delegate: CurrencyTableViewCellDelegate?
    public var currencyName: String? {
        didSet {
            currencyNameLabel.text = currencyName
        }
    }
    
    public var currencyValue: Double? {
        didSet {
            currencyValueTextField.text = String(format:"%f", currencyValue ?? 0.0)
        }
    }
    
    public var canEditCurrencyValue: Bool = false {
        didSet {
            currencyValueTextField.isUserInteractionEnabled = canEditCurrencyValue
        }
    }
    
    //MARK: - Actions
    @IBAction func currencyTextDidChange() {
        delegate?.currencyTableViewCell(self, didChangeCurrencyWith: Double(currencyValueTextField.text!) ?? 0.0)
    }
    
    //MARK: - Override
    override func resignFirstResponder() -> Bool {
        canEditCurrencyValue = false
        currencyValueTextField.resignFirstResponder()
        return true
    }
    
    override func becomeFirstResponder() -> Bool {
        canEditCurrencyValue = true
        currencyValueTextField.becomeFirstResponder()
        return true
    }
}
