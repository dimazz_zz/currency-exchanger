//
//  ViewController.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 26/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class CurrencyExchangerTableViewController: UITableViewController {
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = currencyExchangerViewModel.ratesDataSource
        tableView.delegate = currencyExchangerViewModel.ratesDataDelegate
        currencyExchangerViewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currencyExchangerViewModel.startContiniousFetchingCurrencies(complete: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        currencyExchangerViewModel.pauseFetchingCurrencies()
    }
    
    //MARK: - Private
    fileprivate lazy var currencyExchangerViewModel = CurrencyExchangerViewModel()
}

//MARK: - CurrencyExchangerViewModelDelegate
extension CurrencyExchangerTableViewController: CurrencyExchangerViewModelDelegate {
    func didUpdateRates(_ currencyExchangerViewModel: CurrencyExchangerViewModel) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func didSelectCurrency(_ currencyExchangerViewModel: CurrencyExchangerViewModel) {
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
}

//MARK: - UITextFieldDelegate
extension CurrencyExchangerTableViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currencyExchangerViewModel.pauseFetchingCurrencies()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        currencyExchangerViewModel.resumeFetchingCurrenciesIfNeeded()
        return true
    }
}
