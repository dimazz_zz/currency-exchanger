//
//  CurrencyRatesDataProvider.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 27/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class CurrencyRatesDataProvider: NSObject {
    //MARK: - Public
    func update(with rates: [Currency]) {
        if let _ = allRates {
            rates.forEach({ self.ratesMap[$0.name]?.value = $0.value})
        } else {
            allRates = rates
            rates.forEach({ self.ratesMap[$0.name] = $0})
        }
    }
    
    func moveCurrencyToTop(from index: Int) {
        if let selectedCurrency = allRates?.remove(at: index) {
            allRates?.insert(selectedCurrency, at: 0)
        }
    }
    
    func numberOfCurrencies() -> Int {
        return allRates?.count ?? 0
    }
    
    func currency(at index: Int) -> Currency? {
        return allRates?[index]
    }
    
    func currencyValue(at index: Int) -> Double? {
        return allRates?[index].value
    }
    
    func currencyName(at index: Int) -> String? {
        return allRates?[index].name
    }
    
    func canEditCurrency(at index: Int) -> Bool {
        return index == 0
    }
    
    //MARK: - Private
    private var allRates: [Currency]?
    private var ratesMap = [String : Currency]()
}
