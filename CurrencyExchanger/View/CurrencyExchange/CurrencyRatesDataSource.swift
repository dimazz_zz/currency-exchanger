//
//  CurrencyRatesDataSource.swift
//  CurrencyExchanger
//
//  Created by Dmitry Li on 27/02/2019.
//  Copyright © 2019 KGroup. All rights reserved.
//

import UIKit

class CurrencyRatesDataSource: NSObject {
    //MARK: - Public
    public weak var baseCurrencyDelegate: CurrencyTableViewCellDelegate?
    
    init(with currencyRatesDataProvider: CurrencyRatesDataProvider) {
        self.ratesDataProvider = currencyRatesDataProvider
    }
    
    //MARK: - Private
    fileprivate var ratesDataProvider: CurrencyRatesDataProvider
}

//MARK: - UITableViewDataSource
extension CurrencyRatesDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ratesDataProvider.numberOfCurrencies()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell") as! CurrencyTableViewCell
        cell.currencyName = ratesDataProvider.currencyName(at: indexPath.row)
        cell.currencyValue = ratesDataProvider.currencyValue(at: indexPath.row)
        cell.canEditCurrencyValue = ratesDataProvider.canEditCurrency(at: indexPath.row)
        cell.delegate = baseCurrencyDelegate
        return cell
    }
}
